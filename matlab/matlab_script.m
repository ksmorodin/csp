clc;
clear;
close all;

Str = fileread('../artifacts/signalOutput');
CStr = strsplit(Str,'\n')'

% INPUT PARSING

X = zeros(length(CStr),2)

for x=1:length(CStr)
    tmp = string(CStr(x));
    X(x,:) = str2double(strsplit(tmp,','));
end

% SIGNAL FILTERING
%<->
%signal = X(:,2)
input_signal = X(:,2);
output_signal = X(:,1);

dicretizationFreq = 0.02;

t = 0 : length(input_signal)-1;

t = transpose(t)

filteringRatio = 0.05;

%<->
%signal_filtered = lowpass(signal,filteringRatio);
signal_filtered = lowpass(output_signal, filteringRatio);

f = 1 / dicretizationFreq * filteringRatio * 2 * pi;

lp_filter = tf(1, [1/f 1]);

% VELOCITY

%result_velocity = lsim(lp_filter,signal,t);

velocity = diff(signal_filtered) / 0.02;
%filtered_velocity = diff(signal_filtered) .* 1/50;

%lp_velocity = diff(result_velocity);
%<->
%lp_velocity_filtered = lowpass(lp_velocity,filteringRatio);
lp_velocity_filtered = lowpass(velocity, filteringRatio);

%velocity_twice_filtered = lowpass(filtered_velocity,filteringRatio);

% ACCELERATION

%result_acceleration = lsim(lp_filter,velocity,t(1:580));

%acceleration = diff(velocity) .* 1/50;
%<->
%filtered_acceleration = diff(filtered_velocity) .* 1/50;
filtered_acceleration = diff(lp_velocity_filtered) / 0.02;

%lp_acceleration = diff(result_acceleration);
%<->
%lp_acceleration_filtered = lowpass(lp_acceleration,filteringRatio);
lp_acceleration_filtered = lowpass(filtered_acceleration, filteringRatio);

input_signal = input_signal(1:end-100)
lp_velocity_filtered = lp_velocity_filtered(1:end-100)
signal_filtered = signal_filtered(1:end-100)
lp_acceleration_filtered = lp_acceleration_filtered(1:end-100)
t = t(1:end-100)

acceleration_twice_filtered = lowpass(filtered_acceleration,filteringRatio);

% DEMO

subplot(3,1,1)
hold on, grid on;
plot(t, output_signal(1:end-100));
plot(t, signal_filtered);
%plot(t, result_velocity);
title('SIGNAL');

subplot(3,1,2)
hold on, grid on;
plot(t(2:end),velocity(1:end-100));
plot(t(2:end), lp_velocity_filtered);
%plot(t(2:end),filtered_velocity);
title('VELOCITY');

subplot(3,1,3)
hold on, grid on;
%plot(t(3:end),acceleration);
plot(t(3:end),filtered_acceleration(1:end-100));
plot(t(3:end),lp_acceleration_filtered);
title('ACCELERATION');

figure;
bode(lp_filter);

% IDENTIFICATION

S = [signal_filtered(3:end)'; lp_velocity_filtered(2:end)'; -input_signal(3:end)']' 
P = -pinv(S)*lp_acceleration_filtered;
