clc;
clear;
close all;

Str = fileread('../artifacts/signalSin');
CStr = strsplit(Str,'\n')'

% INPUT PARSING

X = zeros(length(CStr),2)

for x=1:length(CStr)
    tmp = string(CStr(x));
    X(x,:) = str2double(strsplit(tmp,','));
end

R = X(:,1);
Y = X(:,2);
E = 0;
for i=1:length(Y)-1
   E = E + (R(i) - Y(i))^2;
end
figure
hold on, grid on
plot(R);
plot(Y);
E_mean = sqrt(E/length(Y));
    