#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "./regulator/integrator.h"
#include "./lib/controllerbackend.h"
#include "./regulator/pid.h"
#include "./Controller/testcontroller.h"
#include "./regulator/createSinSignal.h"
#include "./regulator/filter.h"
#include "./regulator/logger.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    TestController testController;

    createSinSignal refInput;

    logger filelog;

    Filter filter;


    ControllerBackend controllerBackend(50, "Zsdnw82iuw");


    QQmlApplicationEngine engine;

    PID pid(23.6574802726261, 19.128188013781, 6.17194279719804,723.278026844653);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.rootContext()->setContextProperty("plant", &controllerBackend);



    QObject::connect(&controllerBackend, &ControllerBackend::outputBytesChanged,
                     &filter, &Filter::input);

    QObject::connect(&controllerBackend, &ControllerBackend::outputBytesChanged,
                     &filelog, &logger::signal_log);

    QObject::connect(&filter, &Filter::output,
                     &refInput, &createSinSignal::input);

    QObject::connect(&refInput, &createSinSignal::output,
                        &pid, &PID::input);

    QObject::connect(&refInput, &createSinSignal::for_plant,
                        &controllerBackend, &ControllerBackend::setReferenceSignal);

    QObject::connect(&pid, &PID::output,
                        &controllerBackend, &ControllerBackend::setInput);


    engine.load(url);



    return app.exec();
}
