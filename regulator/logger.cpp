#include "logger.h"

logger::logger() : QObject()
{
    log_file = new QFile("../controltest/artifacts/signalOutput");
    log_stream = new QTextStream(log_file);
}
logger::~logger(){
    log_file->close();
    delete log_file;
    delete log_stream;
}

void logger::signal_log(QByteArray y_bytes){
    int bytes_sum = 0;
    int checksum = (unsigned char)y_bytes[10];
    float part_1; //4 байта
    float part_2;
    for (int i = 0; i < 10; i++){
        bytes_sum += (unsigned char)y_bytes[i];
    }

    if (255-bytes_sum%256 == checksum){
        memcpy(&part_1, y_bytes.data()+ 2 , 4);
        memcpy(&part_2, y_bytes.data()+ 6 , 4);
        *log_stream << part_1 << "," << part_2 << "\n";
    }

}
