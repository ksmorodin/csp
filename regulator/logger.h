#ifndef FILE_LOG_H
#define FILE_LOG_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QIODevice>
#include <QDebug>

class logger : public QObject
{
    Q_OBJECT
public:
    explicit logger();
    ~logger();

private:
    QFile* log_file;
    QTextStream* log_stream;

public slots:
    void signal_log(QByteArray y_bytes);
};

#endif // FILE_LOG_H
