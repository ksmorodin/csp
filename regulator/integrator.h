#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <QObject>
#include "QDateTime"


class Integrator: public QObject
{
    Q_OBJECT
public:
    Integrator(float ic, float prevInput);
    float update(float input, float dt);
    float m_state;
    float prev_t;

private:
    float m_previous_input;


};

#endif // INTEGRATOR_H
