#ifndef REFINPUT_H
#define REFINPUT_H
#include <cmath>
#include <QObject>
#include <ctime>
#include <chrono>
#include <QFile>
#include <QTextStream>

class createSinSignal : public QObject
{
    Q_OBJECT
public:
    createSinSignal();
    ~createSinSignal();

private:
    QFile* log_file;
    QTextStream* log_stream;
    float getRef();
    float refVal;

    std::chrono::high_resolution_clock::time_point t_start;
    std::chrono::high_resolution_clock::time_point t_now;
    float dt;
    float old_dt;


public slots:
    void input(float y);

signals:
    void output(float error, float dt);
    void for_plant(float ref);
};

#endif // REFINPUT_H
