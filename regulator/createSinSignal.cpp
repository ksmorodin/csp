#include "createSinSignal.h"
#include "QDebug"
#include <sys/time.h>
#include <QDateTime>

using namespace std;

createSinSignal::createSinSignal()
{
    log_file = new QFile("../controltest/artifacts/signalSin");
    if (!log_file->open(QIODevice::WriteOnly)){
        qDebug() << "file not opened!";
    }
    log_stream = new QTextStream(log_file);

    this->t_start = chrono::high_resolution_clock::now();
    this->old_dt = 0;
}

float createSinSignal::getRef(){
    this->t_now = chrono::high_resolution_clock::now();


    this->dt = chrono::duration_cast<std::chrono::microseconds>(t_now-t_start)
            .count()/1000000.0;

    float ref = 70.0 + 10 * pow(exp(1), -0.1 * dt)+
            ((-pow(dt-2,2)+60*dt)/100)* sin(dt/2);
    return ref;
};

createSinSignal::~createSinSignal(){
    log_file->close();
    delete log_file;
    delete log_stream;
}

void createSinSignal::input(float y){
    *log_stream << getRef() << "," << y << "\n";
    float temp = old_dt;
    old_dt = dt;
    emit output(getRef() - y, dt - temp);
    emit for_plant(getRef());
}


