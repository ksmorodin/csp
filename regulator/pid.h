#ifndef PID_H
#define PID_H
#include <QObject>
#include "integrator.h"
#include "derivative.h"
class PID : public QObject
{
    Q_OBJECT
public:
    PID(float const Kp,float const Ki, float const Kd, float const N);


private:
    float Kp;
    float Ki;
    float Kd;
    float N;

    Integrator * integrator;
    Integrator * integrator_d;
    Derivative * derivative;
public slots:
    void input(float error , float dt);

signals:
    void output(float u);

};

#endif // PID_H
