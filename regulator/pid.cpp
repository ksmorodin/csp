#include "pid.h"
#include "integrator.h"
#include "derivative.h"
#include "QSignalMapper"
#include "QDateTime"
#include "QDebug"

PID::PID(float const Kp,float const Ki, float const Kd, float const N)
{
    this->Kd = Kd;
    this->Ki = Ki;
    this->Kp = Kp;
    this->N = N;

    integrator = new Integrator(0,0);
    integrator_d = new Integrator(0,0);
    derivative = new Derivative();

}

void PID::input(float error, float dt)
{

    float u = error * this->Kp + this->Ki*integrator->update(error, dt)
            + this->Kd * this->N/(1+N*integrator_d->update(error, dt));

    emit output(u);
}


