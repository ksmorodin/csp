#include "integrator.h"
#include "QDebug"
#include <sys/time.h>
#include <ctime>
#include <chrono>

using namespace std;

Integrator::Integrator(float ic, float prevInput)
{
    m_state = ic;
    this->m_previous_input = prevInput;

}


float Integrator::update(float input, float t)
{

    this->m_state = this->m_state + (m_previous_input + input)*(t)/2;
    m_previous_input = input;

    return this->m_state;
}
