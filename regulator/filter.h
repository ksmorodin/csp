#ifndef FILTER_H
#define FILTER_H

#include <QObject>
#include "integrator.h"

class Filter : public QObject
{
    Q_OBJECT
public:
    Filter();
    ~Filter();

private:
    float y;
    Integrator* m_integrator;
    float time;

public slots:
    void input(QByteArray message);
signals:
    void output(float y_filtered);
};

#endif // FILTER_H
