#ifndef DERIVATIVE_H
#define DERIVATIVE_H

#include <QObject>
#include "QDateTime"

class Derivative : public QObject
{
    Q_OBJECT
public:
    Derivative();
    float update(float state, float t);

private:
    float m_state = 0;
    float prev_t = QDateTime::currentMSecsSinceEpoch()/1000;

};
#endif // DERIVATIVE_H
