#include "filter.h"
#include "QDebug"
#include <QDateTime>

Filter::Filter()
{
    m_integrator = new Integrator(0, 0);
}

Filter::~Filter()
{
    delete m_integrator;
}

void Filter::input(QByteArray y_bytes){
    int sum = 0;
    int check = (unsigned char)y_bytes[10];

    for (int i = 0; i < 10; i++){
        sum += (unsigned char)y_bytes[i];
    }

    time += 0.02;
    if (255-sum%256 == check){
        memcpy(&this->y, y_bytes.data()+ 2 , 4);

        float filtered_y = 50 * m_integrator->update(-50*m_integrator->m_state+y, time);
        time = 0;
        emit output(filtered_y);

    }
}
