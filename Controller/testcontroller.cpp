#include "testcontroller.h"
#include "./lib/controllerbackend.h"

#include <QDebug>
TestController::TestController(QObject *parent) : QObject(parent)
{


}

void TestController::compute()
{
    //emit computed((referenceSignal - value) * 10);
    //emit computed(referenceSignal);
    emit computed(20);
//    emit computed(50 + 50*qSin(QDateTime::currentMSecsSinceEpoch() / 1000));
}



TestController::~TestController(){

}

void TestController::getMessage(QByteArray x){
    int bytes_sum = 0;
    int checksum = (unsigned char)x[10];
    float part_1; //4 байта
    float part_2;
    for (int i = 0; i < 10; i++){
        bytes_sum += (unsigned char)x[i];
    }

    if (255-bytes_sum%256 == checksum){
        memcpy(&part_1, x.data()+ 2 , 4);
        qDebug() << part_1;
        memcpy(&part_2, x.data()+ 6 , 4);

    }
    compute();
}

