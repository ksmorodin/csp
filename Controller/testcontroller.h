#ifndef TESTCONTROLLER_H
#define TESTCONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QtMath>
#include <QFile>
#include <QDateTime>
#include <QTime>

class TestController : public QObject
{
    Q_OBJECT
public:
    explicit TestController(QObject *parent = nullptr);
    ~TestController();
public slots:
    void compute();

    void getMessage(QByteArray x);

signals:
    void computed(float value);


private:
    QFile* file;
    QTextStream* text;
    float part1;
};

#endif // TESTCONTROLLER_H
